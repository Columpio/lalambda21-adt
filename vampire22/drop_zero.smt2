(set-logic HORN)

(declare-datatypes ((Nat 0)) (((S (projS Nat)) (Z))))
(declare-datatypes ((list 0)) (((nil) (cons (head Nat) (tail list)))))

(declare-fun drop (list Nat list) Bool)
(assert (forall ((y list)) (drop y Z y)))
(assert (forall ((x Nat) (y1 Nat) (y list) (r list))
    (=> (drop r x y)
        (drop r (S x) (cons y1 y)))))
(assert (forall ((x Nat)) (drop nil (S x) nil)))

(assert (forall ((xs list) (ys list))
	(=>	(drop ys Z xs) (= xs ys))))
(check-sat)
; (get-model)