(set-logic HORN)
(declare-datatypes ((Nat_0 0)) (((S_0 (projS_0 Nat_0)) (Z_0 ))))
(declare-fun diseqNat_0 (Nat_0 Nat_0) Bool)
(declare-fun projS_1 (Nat_0 Nat_0) Bool)
(declare-fun isS_0 (Nat_0) Bool)
(declare-fun isZ_0 (Nat_0) Bool)
(assert (forall ((x_12 Nat_0) (x_11 Nat_0))
	(=> (= x_12 (S_0 x_11))
	    (projS_1 x_11 x_12))))
(assert (forall ((x_14 Nat_0))
	(isS_0 (S_0 x_14))))
(assert (isZ_0 Z_0))
(assert (forall ((x_15 Nat_0))
	(diseqNat_0 (S_0 x_15) Z_0)))
(assert (forall ((x_16 Nat_0))
	(diseqNat_0 Z_0 (S_0 x_16))))
(assert (forall ((x_17 Nat_0) (x_18 Nat_0))
	(=> (diseqNat_0 x_17 x_18)
	    (diseqNat_0 (S_0 x_17) (S_0 x_18)))))
(declare-datatypes ((list_0 0)) (((nil_0 ) (cons_0 (head_0 Nat_0) (tail_0 list_0)))))
(declare-fun diseqlist_0 (list_0 list_0) Bool)
(declare-fun head_1 (Nat_0 list_0) Bool)
(declare-fun tail_1 (list_0 list_0) Bool)
(declare-fun isnil_0 (list_0) Bool)
(declare-fun iscons_0 (list_0) Bool)
(assert (forall ((x_22 list_0) (x_20 Nat_0) (x_21 list_0))
	(=> (= x_22 (cons_0 x_20 x_21))
	    (head_1 x_20 x_22))))
(assert (forall ((x_22 list_0) (x_20 Nat_0) (x_21 list_0))
	(=> (= x_22 (cons_0 x_20 x_21))
	    (tail_1 x_21 x_22))))
(assert (isnil_0 nil_0))
(assert (forall ((x_23 Nat_0) (x_24 list_0))
	(iscons_0 (cons_0 x_23 x_24))))
(assert (forall ((x_25 Nat_0) (x_26 list_0))
	(diseqlist_0 nil_0 (cons_0 x_25 x_26))))
(assert (forall ((x_27 Nat_0) (x_28 list_0))
	(diseqlist_0 (cons_0 x_27 x_28) nil_0)))
(assert (forall ((x_29 Nat_0) (x_30 list_0) (x_31 Nat_0) (x_32 list_0))
	(=> (diseqNat_0 x_29 x_31)
	    (diseqlist_0 (cons_0 x_29 x_30) (cons_0 x_31 x_32)))))
(assert (forall ((x_29 Nat_0) (x_30 list_0) (x_31 Nat_0) (x_32 list_0))
	(=> (diseqlist_0 x_30 x_32)
	    (diseqlist_0 (cons_0 x_29 x_30) (cons_0 x_31 x_32)))))
(declare-fun drop_0 (list_0 Nat_0 list_0) Bool)
(assert (forall ((x_0 Nat_0) (y_0 list_0) (x_3 list_0))
	(=>	(and (= x_3 y_0)
			(= x_0 Z_0))
		(drop_0 x_3 x_0 y_0))))
(assert (forall ((x_0 Nat_0) (y_0 list_0) (z_1 Nat_0) (x_1 Nat_0) (x_2 list_0) (x_4 list_0) (x_5 list_0))
	(=>	(and (= x_4 x_5)
			(drop_0 x_5 z_1 x_2)
			(= x_0 (S_0 z_1))
			(= y_0 (cons_0 x_1 x_2)))
		(drop_0 x_4 x_0 y_0))))
(assert (forall ((x_0 Nat_0) (y_0 list_0) (z_1 Nat_0) (x_6 list_0))
	(=>	(and (= x_6 nil_0)
			(= x_0 (S_0 z_1))
			(= y_0 nil_0))
		(drop_0 x_6 x_0 y_0))))
(assert (forall ((n_0 Nat_0) (xs_0 list_0) (x_7 list_0) (x_8 list_0) (x_9 list_0))
	(=>	(and (diseqlist_0 x_8 x_9)
			(drop_0 x_7 n_0 xs_0)
			(drop_0 x_8 n_0 x_7)
			(drop_0 x_9 n_0 xs_0))
		false)))
(check-sat)
