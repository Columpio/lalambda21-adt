(set-logic HORN)
(declare-datatype Nat ((Z) (S (prev Nat))))

(declare-fun even (Nat) Bool)
(assert (even Z))
(assert (forall ((x Nat)) (=> (even x) (even (S (S x))))))

(assert (forall ((x Nat)) (=> (and (even x) (even (S x))) false)))

(check-sat)
(get-model)
