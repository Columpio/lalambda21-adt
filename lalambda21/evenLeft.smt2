(set-logic HORN)
(declare-datatype Tree ((leaf) (node (left Tree) (right Tree))))

(declare-fun evenLeftest (Tree) Bool)
(assert (evenLeftest leaf))
(assert (forall ((x Tree) (y Tree) (z Tree)) (=> (evenLeftest x) (evenLeftest (node (node x y) z)))))

(assert (forall ((x Tree) (y Tree)) (=> (and (evenLeftest x) (evenLeftest (node x y))) false)))

(check-sat)
(get-model)
